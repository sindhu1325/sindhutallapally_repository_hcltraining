<%@page import="com.greatlearning.dao.BooksDao"%>
<%@page import="com.greatlearning.bean.Books"%>
<%@page import="java.util.ListIterator"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Like Books Section</title>
</head>
<body>
<h3 align="center" style="background-color:azimuth;">LIKED BOOKS</h3>
<form action="LikedBooksController" method="post"></form>
<div>
<table align="center" cellpadding="7" cellspacing="7">
	<tr>
	        <th>No.</th>
	        <th>BookImage</th>
			<th>BookNo</th>
			<th>BookTitle</th>
			<th>BookCategory</th>
			<th>BookAuthor</th>
	</tr>
<%
if(session.getAttribute("addtoLikedBooks")!=null){
	ListIterator list = ((List)session.getAttribute("addtoLikedBooks")).listIterator();
	BooksDao booksDao = new BooksDao();
	int count=0,noOfBooks=0;
	while(list.hasNext())
	{
		count++;
		int BookNo=Integer.parseInt((String)list.next());
		Books book = booksDao.getBookByBookNo(BookNo);
		noOfBooks++;
%>
		   <tr>
		        <td><%= count %></td>
		        <td><img src="<%=book.getBookImage() %>" style="width:180px; height:160px;"></td>
		        <td><%= book.getBookNo() %></td>
		        <td><%= book.getBookTitle() %></td>
		        <td><%= book.getBookCategory() %></td>
		        <td><%= book.getBookAuthor() %></td>
		   </tr>
		   <%
		      }
	       %>
	       <tr>
	       <td colspan="7">noOfBooks = <%= noOfBooks%></td>
	       <%
	          }
	       %>
	       </tr>
<tr>
<td style="text-align:center" colspan="6"><input type="button" value="logout" 
onclick="location.href='Logout.jsp'"></td>
</tr>
</table>
</div>
</body>
</html>