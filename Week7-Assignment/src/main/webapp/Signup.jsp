<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Sign Up</title>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}
example{
width: 300px;
  border: 15px solid green;
  padding: 50px;
  margin: 20px;

}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: right;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: green;
  color: white;
}
</style>


</head>
<body>
<div align="center">
<div class="topnav">
<a class="active" href="home.jsp" style="float:left">Home</a>
<a href="menu.jsp">Menu</a>
</div>
<div class="example">
<h1>SignUp</h1>
<form action="SignUpController" method="post">
<table>
<tr>
<td>Name : </td>
<td><input type="text" name="name"></td>
</tr>
    <tr>
        <td>EmailId : </td>
        <td><input type="email" name="emailId"></td>
        
    </tr>
    <tr>
        <td>Password : </td>
        <td><input type="password" name="password"></td>
    </tr>  
    <tr>
        <td>Confirm Password : </td>
        <td><input type="password"name="confirmPassword"></td>
    </tr>
    <tr>
        <td><input type="submit" value="Register"></td>
    </tr>
</table>
</form>
 Already User?<a href="login.jsp">Login</a>
 </div>
</div>
</body>
</html>