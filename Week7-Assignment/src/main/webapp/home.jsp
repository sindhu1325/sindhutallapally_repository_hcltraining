<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<style>
h1{
font-style: inherit;
font-family: cursive;
font-stretch: ultra-expanded;
}
body {
background-color:#ffcccc;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: right;
  color: #f2f2f2;
  text-align: center;
  padding: 15px 50px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: green;
  color: white;
}

div.gallery {

  margin: 5px;
  border: 1px solid #ccc;
  float: left;
  width: 180px;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: 7cm;
}

div.desc {
  padding: 15px;
  text-align: center;
}
</style>
</head>
<body style="text-align: center;">
<h1>BOOKESS</h1>
<div class="topnav">
<a class="active" href="#home" style="float:left">Home</a>
<a href="signup.jsp">SignUp</a>
<a href="login.jsp">Login</a>
</div>
<div class="gallery">
    <img src="https://images-na.ssl-images-amazon.com/images/I/71AuVi-QIuL.jpg" alt="Cinque Terre" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>

<div class="gallery">
  <img src="https://encrypted.google.com/books?id=XLfZDwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&h=142&w=100" alt="Forest" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>

<div class="gallery">
    <img src="https://encrypted.google.com/books?id=kLAoswEACAAJ&printsec=frontcover&img=1&zoom=5&h=157&w=100" alt="Northern Lights" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>

<div class="gallery">
    <img src="https://images-na.ssl-images-amazon.com/images/I/711tJ6aX-SL.jpg" alt="Mountains" width="600" height="400">
  <div class="desc">Hello</div>
</div>
<div class="gallery">
    <img src="https://images-na.ssl-images-amazon.com/images/I/71tbalAHYCL.jpg
    " alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://fourminutebooks.com/wp-content/uploads/2016/06/best-motivational-books-29-678x1024.jpg" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://media.theeverygirl.com/wp-content/uploads/2018/12/10-inspirational-books-to-read-this-year-the-everygirl-2.jpg" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://www.jagranjosh.com/imported/images/E/Articles/Self-help-10.jpg" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/fantasy-books-shadow-and-bone-1555433254.jpg" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0i5bQ45c0Xyz_VgoDOVYR7W_gwYpv6NX2sg&usqp=CAU" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://media.npr.org/assets/bakertaylor/covers/t/the-lord-of-the-rings/9780618640157_custom-bd5c36cb700fafac72208e5f622a6d1a9ca85489-s300-c85.jpg" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZNQmWmBCKEU43MITD_cWGFkfGcQxwik-Y5A&usqp=CAU" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://purewows3.imgix.net/images/articles/2017_01/kids_books_white.jpg?auto=format,compress&cs=strip" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1583948523l/52295653._SY475_.jpg" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
<div class="gallery">
    <img src="https://m.media-amazon.com/images/I/51IzeRM6KkL.jpg" alt="Mountains" width="600" height="400">
  <div class="desc">Add a description of the image here</div>
</div>
</body>
</html>