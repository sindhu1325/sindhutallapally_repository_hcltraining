<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
<style>
.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: right;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: green;
  color: white;
}
</style>
<script type="text/javascript">
function validate() {
	var emailId=document.getElementById("EmailId").value;
	var pass=document.getElementById("password").value;
	
	if(emailId==""){
		document.getElementById("emailerror").innerHTML="Please enter Email...";
		return false;
	}
	else{
		document.getElementById("emailerror").innerHTML="";
	}
	if(pass==""){
		document.getElementById("passerror").innerHTML="Please enter password...";
		return false;
	}
	else{
		document.getElementById("passerror").innerHTML="";
	}
	if(pass.length<5 || pass.length>8){
		document.getElementById("passerror").innerHTML="Password should be in betwwen 5  to 8 character..";
		return false;
	}
	else{
		document.getElementById("passerror").innerHTML="";
	}
	return true;
}
</script>
</head>
<body>
<div class="topnav">
<a class="active" href="home.jsp">Home</a>
</div>
<div align="center">
<fieldset>
<%
String status =(String)request.getAttribute("status");
if(status != null){
	out.println(status);
	}
%>
<h1>Login Form</h1>
 <form  onsubmit="return validate()"  action="LoginController" method="post">
              <table>
                    <tr>
                        <td>EmailId : </td>
                        <td><input type="email" id="email"  name="emailid"></td>
                        <td><span style="color:red" id="emailerror" >*</span></td>
                    </tr>
                    
                     <tr>
                        <td>Password : </td>
                        <td><input type="password" id="pass" name="password"></td>
                        <td><span style="color:red" id="passerror">*</span></td>
                    </tr>
                    
                    <tr>
                    <td><input type="submit" value="login"></td>
                    </tr>
              </table>
        </form>
        <br>
        <hr>
        New User?<a href="signup.jsp">Sign Up</a>
      </fieldset>
		
	</div>
</body>
</html>