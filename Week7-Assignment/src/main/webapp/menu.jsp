<%@page import="java.util.Iterator"%>
<%@page import="com.greatlearning.bean.Books"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Bookess Menu</title>
<style>
h1{
font-style: inherit;
font-family: cursive;
font-stretch: ultra-expanded;
}
body {
background-color:#ffcccc;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: right;
  color: #f2f2f2;
  text-align: center;
  padding: 15px 50px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: green;
  color: white;
}

div.gallery {

  margin: 5px;
  border: 1px solid #ccc;
  float: left;
  width: 180px;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: 7cm;
}

div.desc {
  padding: 15px;
  text-align: center;
}
</style>
</head>
<body>
<script>
function addtoReadLaterBooks(BookNo)
    {
	alert("Welcome to this section");
    	$.post("ReadLaterController",{
    	BookNo:BookNo
    	}, function(data,status)
    	{
    		alert(data);
    	});
    	}
</script>
<script>
function addtoLikedBooks(BookNo)
    {
    alert("Welcome");
	    $.post("LikeController",{
        BookNo:BookNo
	    }, function(data,status)
	    {
		    alert(data);
	    });
	    }
</script>
<h1 align="center">Welcome <%=request.getParameter("Name") %> to BOOKESS</h1> 
<div class="topnav">
<a class="active" href="#home" style="float:left">Home</a>
<a href="logout.jsp">Logout</a>
<a href="LikedBooks.jsp"> Loved Books</a><br/>
<a href="readlater.jsp"> Read Later Books</a><br/>
</div>
<form action="BookController" method="post">


<table align="center" cellpadding="7" cellspacing="7" >
<tr>
            <th>BookImage</th>
			<th>BookId</th>
			<th>BookName</th>
			<th>BookType</th>
			<th>BookAuthor</th>
</tr>
<%
	Object obj = session.getAttribute("obj");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
		<td><img src="<%=book.getBookImage() %>" style="width:180px; height:160px;"></td>
		<td><p style="font-size:20px"><%=book.getBookNo() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookTitle() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookCategory() %></td>
		<td><p style="font-size:20px"><%=book.getBookAuthor() %></p></td>
		<td><input style="background-color:aqua;" type="button" value="Add to Liked Books" onclick="addtoLikedBooks('<%= book.getBookNo()%>')"></td>
		<td><input style="background-color:aqua;" type="button" value="Add to Read later section" onclick="addtoReadLaterBooks('<%= book.getBookNo()%>')"></td>
		</tr>
		<%
	}
%>

</table>

</form>
</body>
</html>