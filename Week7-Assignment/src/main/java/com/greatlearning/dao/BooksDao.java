package com.greatlearning.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.Books;
import com.greatlearning.resource.DbResource;

public class BooksDao {


	public int StoreBookDetails(Books book){
		try {
			Connection con=DbResource.getDbConnection();
			PreparedStatement ps=con.prepareStatement("insert into Book values(?,?,?,?,?,?)");
			
			ps.setInt(1,book.getBookNo());
			ps.setString(2,book.getBookTitle());
			ps.setString(3,book.getBookCategory());
			ps.setString(4, book.getBookAuthor());
			ps.setString(5,book.getBookImage());
			
			return ps.executeUpdate();
		}
		catch(Exception e){
			System.out.println("store method exception"+e);
			return 0;
	    }
	}
	public List<Books> retrieveBookDetails(){
		List<Books> listOfBooks=new ArrayList<>();
		try
		{
			Connection con=DbResource.getDbConnection();
			PreparedStatement ps=con.prepareStatement("select * from Book");
			ResultSet rs=ps.executeQuery();
			while(rs.next())
			{
				Books b=new Books();
				b.setBookNo(rs.getInt(1));
				b.setBookTitle(rs.getString(2));
				b.setBookCategory(rs.getString(3));
				b.setBookAuthor(rs.getString(4));
				b.setBookImage(rs.getString(5));
				
				listOfBooks.add(b);
			}
		}
		catch(Exception e){
			System.out.println("store method exception"+e);	
		}
		return listOfBooks;

	}
		public Books getBookByBookNo(int BookNo) {
	    	List<Books> listOfBooks=new ArrayList<>();
	    	try {
	    		Connection con=DbResource.getDbConnection();
				PreparedStatement pstmt = con.prepareStatement("select * from Book where BookNo="+BookNo);
				Books b = new Books();
				ResultSet rs = pstmt.executeQuery();
				 while(rs.next())
				 {						
					b.setBookNo(rs.getInt(1));
					b.setBookTitle(rs.getString("BookTitle"));
					b.setBookCategory(rs.getString("BookCategory"));
					b.setBookAuthor(rs.getString("BookAuthor"));
					b.setBookImage(rs.getString("BookImage"));
				 }
				  return b;
				}catch (Exception e) {
					System.out.println("In store method "+e);
				  }
			        return null;
	    	 }
		
		
	}
