package com.greatlearning.controller;

import java.io.IOException;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatlearning.bean.Books;
import com.greatlearning.service.BooksService;

/**
 * Servlet implementation class BooksController
 */
@WebServlet("/BooksController")
public class BooksController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String BookCategory = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BooksController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		BooksService b=new BooksService();
		List<Books> listOfBooks=b.getAllBookDetails();
		HttpSession hs=request.getSession();
		hs.setAttribute("obj",listOfBooks);
		RequestDispatcher rd= request.getRequestDispatcher("home.jsp");
		rd.forward(request,response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		int BookNo= Integer.parseInt(request.getParameter("BookNo"));
		String BookName=request.getParameter("BookTitle");
		String BookType=request.getParameter("BookCategory");
		String BookAuthor=request.getParameter("BookAuthor");
		String BookImage=request.getParameter("BookImage");

		Books book=new Books();
		book.setBookNo(BookNo);
		book.setBookTitle(BookName);
		book.setBookCategory(BookCategory);
		book.setBookAuthor(BookAuthor);
		book.setBookImage(BookImage);

		BooksService b=new BooksService();
	    String res=b.StoreBookDetails(book);
		   doGet(request,response);
		pw.println(res);
		RequestDispatcher rd=request.getRequestDispatcher("home.jsp");
		rd.include(request,response);
	}

}
