package com.greatlearning.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greatlearning.bean.SignUp;
import com.greatlearning.dao.SignUpDao;

/**
 * Servlet implementation class SignUpController
 */
@WebServlet("/SignUpController")
public class SignUpController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("Name");
		String emailId = request.getParameter("EmailId");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String contact = request.getParameter("contact");
		
		SignUp signup = new SignUp();
		SignUpDao signupDao = new SignUpDao();
		
		String validateUser = signupDao.StoreSignUpDetails(signup);
		response.getWriter().print(validateUser);
		RequestDispatcher rd1= request.getRequestDispatcher("login.jsp");
		rd1.include(request, response);
	        }
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
