package com.greatlearning.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatlearning.bean.Books;
import com.greatlearning.bean.LoginPage;
import com.greatlearning.dao.LoginPageDao;
import com.greatlearning.service.BooksService;

/**
 * Servlet implementation class LoginPageController
 */
@WebServlet("/LoginPageController")
public class LoginPageController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginPageController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BooksService b=new BooksService();
		List<Books> list=b.getAllBookDetails();
		HttpSession hs=request.getSession();
		hs.setAttribute("obj",list);
		RequestDispatcher rd= request.getRequestDispatcher("home.jsp");
		rd.forward(request,response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);

		response.setContentType("text/html");
		PrintWriter pw=response.getWriter();
		String EmailID = request.getParameter("EmailID");
        String password = request.getParameter("password");
        LoginPage login = new LoginPage(EmailID,password);
        LoginPageDao loginDao = new LoginPageDao();
        String validateUser = loginDao.StoreLoginDetails(login);
        HttpSession hs=request.getSession();
	    hs.setAttribute("login",EmailID);
        response.getWriter().print(validateUser);
        	RequestDispatcher r= request.getRequestDispatcher("menu.jsp");
        	r.include(request, response);
        }
	

}
