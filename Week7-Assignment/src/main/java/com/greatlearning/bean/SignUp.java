package com.greatlearning.bean;

public class SignUp {
	private String Name;
	private String EmailId;
	private String password;
	private String ConfirmPassword;
	private String Contact;
	
	public SignUp() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SignUp(String name, String emailId, String password, String confirmPassword, String contact) {
		super();
		Name = name;
		EmailId = emailId;
		this.password = password;
		ConfirmPassword = confirmPassword;
		Contact = contact;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getEmailId() {
		return EmailId;
	}
	public void setEmailId(String emailId) {
		EmailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return ConfirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		ConfirmPassword = confirmPassword;
	}
	public String getContact() {
		return Contact;
	}
	public void setContact(String contact) {
		Contact = contact;
	}
	@Override
	public String toString() {
		return "SignUp [Name=" + Name + ", EmailId=" + EmailId + ", password=" + password + ", ConfirmPassword="
				+ ConfirmPassword + ", Contact=" + Contact + "]";
	}
	
	
	
	

}
