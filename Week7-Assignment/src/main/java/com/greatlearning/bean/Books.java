package com.greatlearning.bean;

public class Books {
	private int BookNo;
	private String BookTitle;
	private String BookAuthor;
	private String BookCategory;
	private String BookImage;
	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Books(int bookNo, String bookTitle, String bookAuthor, String bookCategory, String bookImage) {
		super();
		BookNo = bookNo;
		BookTitle = bookTitle;
		BookAuthor = bookAuthor;
		BookCategory = bookCategory;
		BookImage = bookImage;
	}
	public int getBookNo() {
		return BookNo;
	}
	public void setBookNo(int bookNo) {
		BookNo = bookNo;
	}
	public String getBookTitle() {
		return BookTitle;
	}
	public void setBookTitle(String bookTitle) {
		BookTitle = bookTitle;
	}
	public String getBookAuthor() {
		return BookAuthor;
	}
	public void setBookAuthor(String bookAuthor) {
		BookAuthor = bookAuthor;
	}
	public String getBookCategory() {
		return BookCategory;
	}
	public void setBookCategory(String bookCategory) {
		BookCategory = bookCategory;
	}
	public String getBookImage() {
		return BookImage;
	}
	public void setBookImage(String bookImage) {
		BookImage = bookImage;
	}
	@Override
	public String toString() {
		return "Books [BookNo=" + BookNo + ", BookTitle=" + BookTitle + ", BookAuthor=" + BookAuthor + ", BookCategory="
				+ BookCategory + ", BookImage=" + BookImage + "]";
	}
	
	

}
