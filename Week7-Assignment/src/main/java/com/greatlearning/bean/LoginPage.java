package com.greatlearning.bean;

public class LoginPage {
	private String EmailId;
	private String password;
	public LoginPage() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoginPage(String emailId, String password) {
		super();
		EmailId = emailId;
		this.password = password;
	}
	public String getEmailId() {
		return EmailId;
	}
	public void setEmailId(String emailId) {
		EmailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "LoginPage [EmailId=" + EmailId + ", password=" + password + "]";
	}
	
	
	

}
