create database week7;

use week7;

create table Books(BookNo int primary key,BookTitle varchar(50),BookCategory varchar(50),BookAuthor varchar(50),BookImage varchar(300));

insert into Books values(1,"Full Stack Java ","Educational "," Mayur Ramgir ","https://images-na.ssl-images-amazon.com/images/I/71AuVi-QIuL.jpg ");

insert into Books values( 2,"The Hidden Force ","Educational ","Louis Couperus"," https://pictures.abebooks.com/isbn/9781440073540-us.jpg");

insert into Books values( 3,"Harry Potter And The Cursed Child ","Fantasy ","J.K.Rowling  ","https://encrypted.google.com/books?id=kLAoswEACAAJ&printsec=frontcover&img=1&zoom=5&h=157&w=100 ");

insert into Books values(4 ,"Revolution Twenty20 ","Fiction "," Chetan Bhagat ","https://images-na.ssl-images-amazon.com/images/I/711tJ6aX-SL.jpg ");

insert into Books values( 5,"Ikigai "," Philosophy","Hector Gracia  "," https://images-na.ssl-images-amazon.com/images/I/71tbalAHYCL.jpg");

insert into Books values(6 ,"Zero to One ","Business ","Peter Theil  ","https://fourminutebooks.com/wp-content/uploads/2016/06/best-motivational-books-29-678x1024.jpg ");

insert into Books values(7 ,"Becoming "," Memoir"," Michelle Obama "," https://media.theeverygirl.com/wp-content/uploads/2018/12/10-inspirational-books-to-read-this-year-the-everygirl-2.jpg");

insert into Books values(8 ,"The Monk Who Sold His Ferrari ","Fiction ","Robin Sharma  ","https://www.jagranjosh.com/imported/images/E/Articles/Self-help-10.jpg ");

insert into Books values( 9,"Shadow And Bone ","Fantasy ","Leigh Bardugo  ","https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/fantasy-books-shadow-and-bone-1555433254.jpg ");

insert into Books values(10 ,"Hobbit ","Fantasy Fiction ","J.R.R.Tolkien  ","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0i5bQ45c0Xyz_VgoDOVYR7W_gwYpv6NX2sg&usqp=CAU");

insert into Books values(11 ,"Lord of the Rings ","Adventure Fiction ","J.R.R.Tolkien "," https://media.npr.org/assets/bakertaylor/covers/t/the-lord-of-the-rings/9780618640157_custom-bd5c36cb700fafac72208e5f622a6d1a9ca85489-s300-c85.jpg");

insert into Books values( 12,"The Last Astronaut "," Science Fiction","David Wellington  ","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZNQmWmBCKEU43MITD_cWGFkfGcQxwik-Y5A&usqp=CAU ");

insert into Books values(13 ," Charlottes Web","Childrens Literature","E.B.White ","https://purewows3.imgix.net/images/articles/2017_01/kids_books_white.jpg?auto=format,compress&cs=strip ");

insert into Books values( 14,"Super Boy "," "," Kids "," https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1583948523l/52295653._SY475_.jpg");

insert into Books values(15 ,"Night of Terror ","Horror ","D.Fischer  ","https://m.media-amazon.com/images/I/51IzeRM6KkL.jpg ");


select * from Books;

create table Login(EmailID varchar(50) primary key,password char(30));

select * from Login;

create table Signup(Name varchar(30) primary key,EmailId varchar(50),password varchar(32),confirmPassword char(32),contact varchar(30));