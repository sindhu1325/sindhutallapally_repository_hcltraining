package com.hcl.greatlearning.week3assignment;
import java.util.*;

public class MagicOfBooks {
	Scanner scanner=new Scanner(System.in);
	HashMap<Integer,BookDetails> bookmap=new HashMap<>();
	TreeMap<Double,BookDetails> treemap=new TreeMap<>();
	ArrayList<BookDetails> booklist=new ArrayList<>();
	public  void addbook(){
		BookDetails b= new BookDetails();
		System.out.println("Enter book id");
		b.setId(scanner.nextInt());
	
		System.out.println("Enter a book name");
		b.setName(scanner.next());
		
		System.out.println("Enter a book price");
		b.setPrice(scanner.nextDouble());
		
		System.out.println("Enter a book genre");
		b.setGenre(scanner.next());
		
		System.out.println("Enter number of copies sold");
		b.setNoOfCopiesSold(scanner.nextInt());	
		
		System.out.println("Enter a book status");
		b.setBookstatus(scanner.next());
		
		bookmap.put(b.getId(), b);
		System.out.println("Book added successfully");
			
		treemap.put(b.getPrice(), b);
		booklist.add(b);
			
	    }
	    
	    public void deletebook() {
	    	
	    	System.out.println("Enter book Id you want to delete");
	    	int id=scanner.nextInt();
	    	bookmap.remove(id);
	    	}
	    
	    public void updatebook() {
	    	BookDetails b= new BookDetails();
	    	System.out.println("Enter the book Id:");
	    	b.setId(scanner.nextInt());
	    	
			System.out.println("Enter the book name:");
			b.setName(scanner.next());

			System.out.println("Enter the book price:");
			b.setPrice(scanner.nextDouble());

			System.out.println("Enter book genre:");
			b.setGenre(scanner.next());
			
			System.out.println("Enter number of copies sold:");
			b.setNoOfCopiesSold(scanner.nextInt());
			
			System.out.println("Enter book status:");
			b.setBookstatus(scanner.next());
			
			bookmap.replace(b.getId(), b);
			System.out.println("Book details Updated successfully");
			}
	    
		public void displayBookInfo() {
			if (bookmap.size() > 0) 
			{
				Set<Integer> keySet = bookmap.keySet();
				for (Integer key : keySet) {
					System.out.println(key + " ----> " + bookmap.get(key));
				}
			} else {
				System.out.println("booksMap is Empty");
			}
		}
		public void count() {			
			System.out.println("Number of books present in a store : "+bookmap.size());
			}	
		public void autobiography() {
			String bestSelling = "Autobiography";
	        ArrayList<BookDetails> genreBookList = new ArrayList<BookDetails>();
	        Iterator<BookDetails> iter=(booklist.iterator());
	        while(iter.hasNext())
	        {
	        	BookDetails b1=(BookDetails)iter.next();
	        	if(b1.genre.equals(bestSelling)) 
	        	{
	        		genreBookList.add(b1);
	        		System.out.println(genreBookList);
	        		}
	        	}  
	        }
		public void displayByFeature(int flag) {	
		if(flag==1){
			if (treemap.size() > 0) {
				Set<Double> keySet = treemap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " ----> " + treemap.get(key));
					}
				}
			else {
				System.out.println("TreeMap is Empty");
				}
			}
		if(flag==2) {
			Map<Double, Object> treeMapReverseOrder= new TreeMap<>(treemap);
			NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();
			System.out.println("Book Details:");
			if (nmap.size() > 0) {
				Set<Double> keySet = nmap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " ----> " + nmap.get(key));
					}
				}
			else{
				System.out.println("TreeMap is Empty");
				}
			}
		if(flag==3) {
			String bestSelling = "B";
			ArrayList<BookDetails> genreBookList = new ArrayList<BookDetails>();      
			Iterator<BookDetails> iter=booklist.iterator();
			while(iter.hasNext())
			{
				BookDetails b=(BookDetails)iter.next();
				if(b.bookstatus.equals(bestSelling)) {
					genreBookList.add(b);
					}
				}
			System.out.println(genreBookList);
			}
		}
}


