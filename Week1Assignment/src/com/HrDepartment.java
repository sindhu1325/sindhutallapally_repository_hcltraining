package com;

public class HrDepartment extends SuperDepartment{
	public String departmentName()
	{
		return "HR Department";
	}
	public String getTodaysWork()
	{
		return "Fill todays worksheet and mark your attendance";
	}
	public String  getWorkDeadline()
	{
		return "Complete by EOD";
	}
	public String getActivity()
	{
		return "Team Lunch";
				
	}
	public String isTodayAHoliday() {
		return " Today is not a holiday ";
		}
	public void display() {
		System.out.println(departmentName());
		System.out.println(getTodaysWork());
		System.out.println(getWorkDeadline());
		System.out.println(getActivity());
		System.out.println(isTodayAHoliday());
		
	}

}
