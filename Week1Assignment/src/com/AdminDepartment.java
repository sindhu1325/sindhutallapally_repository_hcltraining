package com;

public class AdminDepartment extends SuperDepartment{

	public String departmentName()
	{
		return "Admin Department";
	}
	public String getTodaysWork()
	{
		return "Complete your documents Submission";
	}
	public String  getWorkDeadline()
	{
		return "Complete by EOD";
	}
	public String isTodayAHoliday() {
		return " Today is not a holiday ";
		}
	public void display() {
		System.out.println(departmentName());
		System.out.println(getTodaysWork());
		System.out.println(getWorkDeadline());
		System.out.println(isTodayAHoliday());
	}
}
