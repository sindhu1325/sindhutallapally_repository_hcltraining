package com;
import java.util.*;

public class AssignmentMain {

	public static void main(String[] args) {
		SuperDepartment sd= new SuperDepartment();
		sd.display();
		System.out.println();
		AdminDepartment ad= new AdminDepartment();
		ad.display();
		System.out.println();
		HrDepartment hd= new HrDepartment();
		hd.display();
		System.out.println();
		TechDepartment td= new TechDepartment();
		td.display();
		
		
	}

}
