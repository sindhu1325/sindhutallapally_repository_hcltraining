package com.week11.greatlearning.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.week11.greatlearning.bean.Book;
import com.week11.greatlearning.dao.BookDao;

@Service
public class BookService {
	
@Autowired
BookDao bookdao;
public List<Book> getAllBooks() 
{
  return bookdao.retrieveBookDetails();
}
public Book getBookbyBookId(int BookId) {
	return bookdao.getBookByBookId(BookId);
}
public String addtoLikedBooks(Book book) {
	return bookdao.addtoLikedBooks(book);
}

}
