package com.week11.greatlearning.controllers;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.week11.greatlearning.service.BookService;
import com.week11.greatlearning.bean.Book;
@Controller
public class BookController {
	@Autowired
	BookService bookservice;
	@GetMapping(value = "displayDashboard")
	public ModelAndView getAllBooks(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		List<Book> listOfBooks = bookservice.getAllBooks();
		req.setAttribute("books", listOfBooks);
		req.setAttribute("obj"," ");
		mav.setViewName("displayDashboard.jsp");
		return mav;
	}
	@RequestMapping("/BookStore")  
    public ModelAndView BookStore()  
    {  
		ModelAndView mv = new ModelAndView();
		mv.setViewName("BookStore.jsp");
		List<Book> listOfBooks = bookservice.getAllBooks();
		mv.addObject("books",listOfBooks);
        return mv;  
    } 
	
    } 

