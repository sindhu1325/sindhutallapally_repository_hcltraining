package com.greatlearning.model;

public class Book {
	private int BookId;
	private String BookName;
	private String BookCategory;
	private String BookAuthor;
	private String BookImage;
	public int getBookId() {
		return BookId;
	}
	public void setBookId(int bookId) {
		BookId = bookId;
	}
	public String getBookName() {
		return BookName;
	}
	public void setBookName(String bookName) {
		BookName = bookName;
	}
	public String getBookCategory() {
		return BookCategory;
	}
	public void setBookCategory(String bookCategory) {
		BookCategory = bookCategory;
	}
	public String getBookAuthor() {
		return BookAuthor;
	}
	public void setBookAuthor(String bookAuthor) {
		BookAuthor = bookAuthor;
	}
	public String getBookImage() {
		return BookImage;
	}
	public void setBookImage(String bookImage) {
		BookImage = bookImage;
	}
	

}
