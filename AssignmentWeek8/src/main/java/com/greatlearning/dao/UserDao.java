package com.greatlearning.dao;

import com.greatlearning.model.Login;
import com.greatlearning.model.User;

public interface UserDao {
	
	int register(User user);
	
	User validateUser(Login login);

}
