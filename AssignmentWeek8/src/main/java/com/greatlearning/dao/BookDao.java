package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.connection.DatabaseResource;
import com.greatlearning.model.Book;

public class BookDao {
	public Book getBookByBookId(int BookId) {
    	List<Book> listOfBooks=new ArrayList<>();
    	try {
    		Connection con=DatabaseResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement(""+BookId);
			Book p = new Book();
			ResultSet rs = pstmt.executeQuery();
			 while(rs.next())
			 {						
				p.setBookId(rs.getInt(1));
				p.setBookName(rs.getString("BookName"));
				p.setBookCategory(rs.getString("BookCategory"));
				p.setBookAuthor(rs.getString("BookAuthor"));
				p.setBookImage(rs.getString("BookImage"));
			 }
			  return p;
			}catch (Exception e) {
				System.out.println("In store method "+e);
			  }
		        return null;
    	 }

}
