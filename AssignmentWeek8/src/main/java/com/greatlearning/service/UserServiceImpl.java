package com.greatlearning.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.greatlearning.dao.UserDao;
import com.greatlearning.model.Login;
import com.greatlearning.model.User;

public class UserServiceImpl implements UserService{
	@Autowired
	public UserDao userDao;

	  public int register(User user) {
	    return userDao.register(user);
	  }

	  public User validateUser(Login login) {
	    return userDao.validateUser(login);
	  }

}
