package com.greatlearning.service;

import com.greatlearning.model.Login;
import com.greatlearning.model.User;

public interface UserService {
	
	int register(User user);
	
	User validateUser(Login login);

}
