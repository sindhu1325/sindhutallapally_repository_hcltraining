package com.greatlearning.Assessment9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assessment9Application {

	public static void main(String[] args) {
		SpringApplication.run(Assessment9Application.class, args);
		System.err.println("Server running on port number : 9090");
	}

}
