package com.greatlearning.Assessment9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.Assessment9.bean.Book;
@Repository
public interface BookDao extends JpaRepository<Book, Integer>{

}
