package com.greatlearning.Assessment9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.Assessment9.bean.SignUp;

@Repository
public interface SignUpDao extends JpaRepository<SignUp, Integer> {

}
