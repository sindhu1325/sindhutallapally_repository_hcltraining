package com.greatlearning.Assessment9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.Assessment9.bean.Book;
import com.greatlearning.Assessment9.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@GetMapping(value = "getAllBooks",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBookDetails(){
		return bookService.getAllBooks();
	}
	
	@GetMapping(value = "{id}")
	public Book getBook(@PathVariable("id") int bookid) {
		return bookService.getBookById(bookid);
	}
	
	@PostMapping(value = "addBook",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeProductInfo(@RequestBody Book book) {
		return bookService.storeBookDetails(book);
	}
	
	@DeleteMapping(value = "deleteBook/{id}")
	public String storeProductInfo(@PathVariable("id") int BookId) {
		return bookService.deleteBooks(BookId);
	}
	
	@PatchMapping(value = "updateBook")
	public String updateProductInfo(@RequestBody Book book) {
		return bookService.updateBooks(book);
	}

}
