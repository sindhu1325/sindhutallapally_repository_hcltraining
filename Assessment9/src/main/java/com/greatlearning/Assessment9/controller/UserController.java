package com.greatlearning.Assessment9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.Assessment9.bean.User;
import com.greatlearning.Assessment9.service.UserService;
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserService userService;
	
	@PostMapping(value = "addUser",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User user) {
		return userService.storeUserDetails(user);
	}
	
	@GetMapping(value = "getAllUsers",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUserDetails(){
		return userService.getAllUser();
	}
	
	@DeleteMapping(value = "deleteUser/{id}")
	public String deleteUserInfo(@PathVariable("id") int id) {
		return userService.deleteUserDetails(id);
	}
	
	@PatchMapping(value = "updateUser")
	public String updateUserInfo(@RequestBody User user) {
		return userService.updateUserDetails(user);
	}
	
	@GetMapping(value = "{id}")
	public User getUser(@PathVariable("id") int userid) {
		return userService.getUserById(userid);
	}

}
