package com.greatlearning.Assessment9.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.Assessment9.service.LogoutService;

@RestController
@RequestMapping("/login")
public class LogoutController {
	@Autowired
	LogoutService logoutService;
	@DeleteMapping(value = "Logout/{loginid}")
	public String storeLoginInfo(@PathVariable("loginid") int loginid)
	{
	return logoutService.deleteLoginDetails(loginid);
	}

}
