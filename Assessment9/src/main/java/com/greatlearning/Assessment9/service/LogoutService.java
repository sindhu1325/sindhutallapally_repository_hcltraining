package com.greatlearning.Assessment9.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.Assessment9.dao.LoginDao;

@Service
public class LogoutService {
	@Autowired
	LoginDao loginDao;
	public String deleteLoginDetails(int loginid)
	{
		if(!loginDao.existsById(loginid)) {
			return "login id is not present";
			}else {
			loginDao.deleteById(loginid);
			return "Logout successfull...";
			}	
	}

}
