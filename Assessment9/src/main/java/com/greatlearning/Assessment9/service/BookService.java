package com.greatlearning.Assessment9.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.Assessment9.bean.Book;
import com.greatlearning.Assessment9.dao.BookDao;
@Service
public class BookService {
	@Autowired
	BookDao bookDao;
	public String storeBookDetails(Book book) {
		if(bookDao.existsById(book.getId())) {
			return "BookId already exists";
		}else {
			bookDao.save(book);
			return "Books stored";
		}
	}
	
	public Book getBookById(int id) {
		return bookDao.findById(id).get();
	}
	
	public List<Book> getAllBooks(){
		return bookDao.findAll();
	}
	
	public String deleteBooks(int BookId) {
		if(!bookDao.existsById(BookId)) {
			return "No books are present";
		}else {
			bookDao.deleteById(BookId);
			return "Book deleted successfully!!";
		}
	}
	
	public String updateBooks(Book book) {
		if(!bookDao.existsById(book.getId())) {
			return "No books are present with Id";
		}else {
			Book bk = bookDao.getById(book.getId());
			bk.setBook_name(book.getBook_name());
			bk.setAuthor_name(book.getAuthor_name());
			bk.setPrice(book.getPrice());
			
			bookDao.saveAndFlush(bk);
			
			return "Book Updated !!!";
		}
	}

}
