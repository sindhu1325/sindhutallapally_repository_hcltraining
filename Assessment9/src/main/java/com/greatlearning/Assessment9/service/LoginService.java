package com.greatlearning.Assessment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.Assessment9.bean.Login;
import com.greatlearning.Assessment9.dao.LoginDao;

@Service
public class LoginService {
	@Autowired
	LoginDao loginDao;
	public String storeLoginDetails(Login login)
	{	
		if(loginDao.existsById(login.getLoginId())) 	
		{
		  return "Try again..!!";
		}else {
		  loginDao.save(login);
		  return "login successfull";
		}
    }
	public List<Login> getAllLoginDetails(){
		return loginDao.findAll();
	}
	

}
