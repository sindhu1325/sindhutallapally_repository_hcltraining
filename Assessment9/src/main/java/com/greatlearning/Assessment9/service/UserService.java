package com.greatlearning.Assessment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.Assessment9.bean.Book;
import com.greatlearning.Assessment9.bean.User;
import com.greatlearning.Assessment9.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;
	
	public String storeUserDetails(User user) {
		if(!userDao.existsById(user.getId())) {
			return "Id already exists";
		}else {
			userDao.save(user);
			return "User details stored !!!";
		}
			
	}
	
	public List<User> getAllUser(){
		return userDao.findAll();
	}
	
	public String deleteUserDetails(int id) {
		if(!userDao.existsById(id)) {
			return "No user is present with this id";
		}else {
			userDao.deleteById(id);
			return "User Deleted !!!";
		}
	}
	
	public String updateUserDetails(User user) {
		if(!userDao.existsById(user.getId())) {
			return "No User is present !!";
		}else {
			User ur = userDao.getById(user.getId());
			ur.setUsername(user.getUsername());
			ur.setPassword(user.getPassword());
			
			userDao.saveAndFlush(ur);
			return "User details Updated !!!";
		}
	}
	
	public User getUserById(int id) {
		return userDao.findById(id).get();
	}

}
