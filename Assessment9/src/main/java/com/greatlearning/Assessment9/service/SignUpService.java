package com.greatlearning.Assessment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.Assessment9.bean.SignUp;
import com.greatlearning.Assessment9.dao.SignUpDao;

@Service
public class SignUpService {
	@Autowired
	SignUpDao signupDao;
	
	public String storeSignUpDetails(SignUp signup)
	{	
		if(signupDao.existsById(signup.getSid())) 
		{
		  return "Try again..!!";
		}else {
		  signupDao.save(signup);
		  return "SignUp Successfull ...";
		}
    }
	public List<SignUp> getAllSignUpDetails(){
		return signupDao.findAll();
	}
	

}
