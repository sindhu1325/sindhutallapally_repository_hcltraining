 create database week9_sindhu;
use week9_sindhu;

create table book( id int primary key, book_name varchar(20), author_name varchar(20) , price float);

create table user(id int primary key, username varchar(30), password varchar(20));

create table signup(sid int primary key, name varchar(30), email varchar(50),password varchar(20));

create table login(login_id int primary key,email_id varchar(255),password varchar(255),username varchar(255));

select * from book;

select * from user;

select * from signup;

select * from login;
