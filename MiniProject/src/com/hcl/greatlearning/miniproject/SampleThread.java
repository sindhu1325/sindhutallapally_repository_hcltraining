package com.hcl.greatlearning.miniproject;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Date;
import java.util.List;

public class SampleThread extends Thread {
	@SuppressWarnings("deprecation")
	public void run() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		@SuppressWarnings("unused")
		Date time = new Date();
		List<Billing> billing = new ArrayList<Billing>();
		boolean finalOrder;
		
		List<Items> items = new ArrayList<Items>();
		Items i1 = new Items(1, "Rajma Chawal", 2, 150.0);
		Items i2 = new Items(2, "Chicken Biryani", 1, 230.0);
		Items i3 = new Items(3, "Chilli Potato", 2, 250.0);
		Items i4 = new Items(4, "Butter Naan", 2, 150.0);
		Items i5 = new Items(5, "Paneer Curry", 1, 210.0);
		
		items.add(i1);
		items.add(i2);
		items.add(i3);
		items.add(i4);
		items.add(i5);
		
		System.out.println("Welcome to Surabi Restaurant\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		while (true) {
			System.out.println("Please Enter the Credentials");
			System.out.println("Email : ");
			@SuppressWarnings("unused")
			String email = sc.next();
			System.out.println("Password : ");
			String password = sc.next();
			String name = Character.toUpperCase(password.charAt(0))+ password.substring(1);
			System.out.println("Please Enter A-if you are Admin and U-if you are User,L to logout");
			String aorU = sc.next();
			Billing bill = new Billing();
			List<Items> selectedItems = new ArrayList<Items>();
			
			double totalCost = 0;
			Date date = new Date();
			
			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter tf = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy");
			@SuppressWarnings("unused")
			String currentTime = time1.format(tf);
			if (aorU.equals("U") || aorU.equals("u")) {
				System.out.println("Welcome Mr/Mrs."+name);
				do {
					System.out.println("Today's Menu :");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item Code:");
					int code = sc.nextInt();
					if (code == 1) {
						selectedItems.add(i1);
						totalCost += i1.getItemPrice();	
					}
					else if (code == 2) {
						selectedItems.add(i2);
						totalCost += i2.getItemPrice();	
					}
					else if (code == 3) {
						selectedItems.add(i3);
						totalCost += i3.getItemPrice();	
					}
					else if (code == 4) {
						selectedItems.add(i4);
						totalCost += i4.getItemPrice();	
					}
					else {
						selectedItems.add(i5);
						totalCost += i5.getItemPrice();	
					}
					System.out.println("Press 0 to show Bill\nPress 1 to order more");
					int opt = sc.nextInt();
					if (opt == 0) 
						finalOrder = false;
					else
							finalOrder = true;					
				} while (finalOrder);
				System.out.println("Thanks Mr "+ name + "for visiting Surabi");
				System.out.println("Items you have selected");
				selectedItems.stream().forEach(e -> System.out.println(e));
				System.out.println("Total Bill : "+totalCost);
				
				bill.setName(name);
				bill.setCost(totalCost);
				bill.setItems(selectedItems);
				bill.setTime(date);
				billing.add(bill);
		} else if(aorU.equals("A") || aorU.equals("a")){
			System.out.println("Welcome Admin");
			System.out.println("Press 1 to see all Bills today\nPress 2 to see all Bills of this month\nPress 3 to see all the Bills");
			int option = sc.nextInt();
			switch (option) {
			case 1: 
			if (!billing.isEmpty()) {
				for (Billing b:billing) { 
				if(b.getTime().getDate() == time.getDate()){
						System.out.println("\nUsername :- " + b.getName());
						System.out.println("Items :- " + b.getItems());
						System.out.println("Total :- " + b.getCost());
						System.out.println("Date " + b.getTime() + "\n");
					}
			}
			}
				 else
					System.out.println("No Bills today.!");
				break;

			case 2:
				if (!billing.isEmpty()) {
					for (Billing b : billing){
						if(b.getTime().getMonth() == time.getMonth()) {
						System.out.println("\nUsername :- " + b.getName());
						System.out.println("Items :- " + b.getItems());
						System.out.println("Total :- " + b.getCost());
						System.out.println("Date " + b.getTime() + "\n");
					}
					}
				} else
					System.out.println("No Bills for this month.!");
				break;

			case 3:
				if (!billing.isEmpty()) {
					for (Billing b : billing) {
						System.out.println("\nUsername :- " + b.getName());
						System.out.println("Items :- " + b.getItems());
						System.out.println("Total :- " + b.getCost());
						System.out.println("Date " + b.getTime() + "\n");
					}
				} else
					System.out.println("No Bills.!");
				break;
				default:
					System.out.println("Invalid Option");
					System.exit(1);
					}
			}
		else if (aorU.equals("L") || aorU.equals("l")) {
			System.exit(1);
			}else  {
				System.out.println("Invalid Entry");
				}
			}
		}
		
	}


