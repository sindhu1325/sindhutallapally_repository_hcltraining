package com.hcl.greatlearning.miniproject;

public class Items {
	private int itemId;
	private String itemName;
	private int itemQuantity;
	private double itemPrice;
	
	public Items(int itemId, String itemName, int itemQuantity, double itemPrice)throws IllegalArgumentException {
		super();
		if(itemId<0) {
			throw new IllegalArgumentException("Exception occured,Id cannot be less than zero");
		}
		this.itemId=itemId;
		if(itemName==null) {
			throw new IllegalArgumentException("Exception occured,Name cannot be null");
		}
		this.itemName=itemName;
		if(itemQuantity<0) {
			throw new IllegalArgumentException("Exception occured,Quantity cannot be less than zero");
		}
		this.itemQuantity=itemQuantity;
		if(itemPrice<0) {
			throw new IllegalArgumentException("Exception occured,Price cannot be less than zero ");
		}
		this.itemPrice=itemPrice;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return " "+itemId+" "+itemName+" "+itemQuantity+" "+itemPrice;
	}
}
