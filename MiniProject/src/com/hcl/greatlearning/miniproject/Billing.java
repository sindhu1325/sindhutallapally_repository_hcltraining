package com.hcl.greatlearning.miniproject;

import java.util.*;
import java.time.LocalDateTime;

public class Billing {
	private String name;
	private List<Items> items;
	private double cost;
	private Date time;
	
	public Billing() {}
	
	public Billing(String name,List<Items> items, double cost,Date time) throws IllegalArgumentException {
		super();
		this.name = name;
		this.items = items;
		if(cost<0) {
			throw new IllegalArgumentException("Exception occured, Cost cannot be less than zero");
		}
		this.cost = cost;
		this.time = time;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Items> getItems() {
		return items;
	}
	public void setItems(List<Items> selectedItems) {
		this.items = selectedItems;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date date) {
		this.time = date;
	}	
}
