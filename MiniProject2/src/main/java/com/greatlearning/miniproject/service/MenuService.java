package com.greatlearning.miniproject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.miniproject.bean.*;
import com.greatlearning.miniproject.dao.*;

@Service
public class MenuService {
	@Autowired
	MenuDao menuDao;

	public List<Menu> getAllMenu() {
		return menuDao.findAll();
	}

	public String addNewMenu(Menu menu) {
		if (menuDao.existsById(menu.getItemId())) {
			return "ID must be unique ";
		} else {
			menuDao.save(menu);
			return "New Item Added Successfully";
		}

	}

	public String updateItemPrice(Menu menu) {
		if (!menuDao.existsById(menu.getItemId())) {
			return "Details not present. Re-Enter!!";
		} else {
			Menu m = menuDao.getById(menu.getItemId());
			m.setItemPrice(menu.getItemPrice());
			menuDao.saveAndFlush(m);
			return "Price Updated SuccessFully";
		}
	}

	public String deleteItem(int id) {
		if (!menuDao.existsById(id)) {
			return "Details not present.Enter Correct ID";
		} else {
			menuDao.deleteById(id);
			return "Deleted Successfully.";
		}

	}

}
