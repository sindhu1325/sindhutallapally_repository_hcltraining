package abc;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Comparator;


public class Main {

	public static void main(String[] args) {
		ArrayList<Employee> employees = new ArrayList<>();
		employees.add(new Employee(1,"Aman",20,1100000,"IT","Delhi"));
		employees.add(new Employee(2,"Bobby", 22 ,500000,"HR","Bombay"));
		employees.add(new Employee(3,"Zoe",20,750000,"Admin","Delhi"));
		employees.add(new Employee(4,"Smitha",21,-1000000,"IT","Chennai"));
		employees.add(new Employee(5,"Smitha",24,1200000,"HR","Bengaluru"));
		
		
		System.out.printf("%1s %7s %7s %15s %15s %14s","S.No.","Name","Age","Salary(INR)","Department","Location\n");
		System.out.println();
		for(Employee e1:employees) {
			System.out.format("%1s %11s %7s %15s %14s %14s",e1.getId(),e1.getName(),e1.getAge(),e1.getSalary(),e1.getDepartment(),e1.getCity());
			System.out.println();
		}
		
		
		
		System.out.println("\n");
		DataStructureB b =new DataStructureB();
		System.out.println("Count of Employees from each city:");
		b.cityNameCount(employees);
		
				
		System.out.println("\n");
		System.out.println("Monthly Salaries of all Employees are:");
		b.monthlySalary(employees);
		System.out.println("\n");
		
		Collections.sort(employees, new DataStructureA());   // using Comparator 
		System.out.println("Names of Employees in Sorted Order:");
		for(Employee e1:employees) {
			System.out.print(e1+" ");
		}
			
		}
		
			
	}

